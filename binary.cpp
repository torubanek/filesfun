#include <iostream>
#include<fstream>
using namespace std;

int main( )
{
   //create some data
   short int a = -6730;
   float b = 68.123; 
   char c = 'J';

    ofstream myfile{"abc.bin", ios::binary};//try to open abc.bin for writing (output) in
    if (myfile)// check if file opened successfully
    {
        myfile.write(reinterpret_cast<char*>(&a), sizeof(a));
        myfile.write(reinterpret_cast<char*>(&b), sizeof(b));
        myfile.write(reinterpret_cast<char*>(&c), sizeof(c));
    }
    else std::cout<<"Error opening abc.bin"<<endl;//print error before program quits
    myfile.close();
    
   cout<<a<<" "<<b<<" "<<c;//display data to console (i.e. as formatted chars)

   cout<<endl<<endl;
 
   return 0;
}

/*
   b: binary=0x7fffffffdfac:	11111010	00111110
      hex   =0x7fffffffdfac:	0xfa	0x3e

   c: binary=0x7fffffffdfa9:	01001010	10110110
      hex   =0x7fffffffdfa9:	0x4a	0xb6
*/
